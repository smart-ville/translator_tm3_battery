/*
 * BMSconfig.h
 *
 *  Created on: Oct 29, 2021
 *      Author: smartville
 */

#ifndef PERIPHERALS_BMSCONFIG_H_
#define PERIPHERALS_BMSCONFIG_H_

#define STRING_ID 101
#define CODE_ACTIVATION 1
#define TOTAL_MODULES 96
#define StrCaC 173.00 //Initial charge capacity of the battery
#define StrCaE 65.00 //Initial energy capacity of the battery in kWh
#define StrPnominal 250 //Nominal power in kW// Maximum discharge power capacity in full power condition
#define charge_current_limit 200//Initial charge current limit
#define initial_SOC 50



//Safety definitions
#define TEMP_OVER_ALARM 55
#define TEMP_OVER_WARNING 50
#define TEMP_UNDER_ALARM -20
#define TEMP_UNDER_WARNING -15
#define CURRENT_OVER_CHARGE_ALARM 80
#define CURRENT_OVER_CHARGE_WARNING 60
#define CURRENT_OVER_DISCHARGE_ALARM -100
#define CURRENT_OVER_DISCHARGE_WARNING -80
#define PACK_VOLTAGE_OVER_ALARM 415
#define PACK_VOLTAGE_OVER_WARNING 410
#define PACK_VOLTAGE_UNDER_ALARM 290
#define PACK_VOLTAGE_UNDER_WARNING 300
#define CELL_VOLTAGE_OVER_ALARM 4.15
#define CELL_VOLTAGE_OVER_WARNING 4.10
#define CELL_VOLTAGE_UNDER_ALARM 2.90
#define CELL_VOLTAGE_UNDER_WARNING 3.00
#define SOC_OVER_ALARM 97
#define SOC_OVER_WARNING 95
#define SOC_UNDER_ALARM 5
#define SOC_UNDER_WARNING 10

#endif /* PERIPHERALS_BMSCONFIG_H_ */
