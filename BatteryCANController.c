//This program is for translator and controller for Tesla Model 3 battery
//Author: S M Rakiul Islam
//Smartville Inc
//Date: 11/01/2021


#include "DSP28x_Project.h"     // Device Headerfile and Examples Include File
#include <string.h>
#include "Peripherals/timersetup.h"
#include "Peripherals/GPIOsetup.h"
#include "Peripherals/CANcontroller.h"

#include "Peripherals/BMSconfig.h"
#include "Peripherals/datamodel.h"
#include "Peripherals/TM3.h"
#include "Peripherals/UpperCAN.h"
#include "Peripherals/LowerCAN.h"
#include "Peripherals/memmap.h"


//
// Main
//
void main(void)
{

    InitSysCtrl();
    DINT;
    InitPieCtrl();
    IER = 0x0000;
    IFR = 0x0000;
    InitPieVectTable();

    init_data_model();
    init_UPPER_CAN();
    init_LOWER_CAN();
    init_timer0();
    init_GPIO();

    //
    // Interrupts that are used in this example are re-mapped to
    // ISR functions found within this file.
    //
    EALLOW;         // This is needed to write to EALLOW protected registers
    PieVectTable.TINT0 = &cpu_timer0_isr;
    EDIS;    // This is needed to disable write to EALLOW protected registers



    programinflash();//Flash programming

    //
    // Enable CPU INT1 which is connected to CPU-Timer 0:
    //
    IER |= M_INT1;

    //
    // Enable TINT0 in the PIE: Group 1 interrupt 7
    //
    PieCtrlRegs.PIEIER1.bit.INTx7 = 1;

    //
    // Enable global Interrupts and higher priority real-time debug events
    //
    EINT;           // Enable Global interrupt INTM
    ERTM;           // Enable Global realtime interrupt DBGM

    //
    // Step 6. IDLE loop. Just sit and loop forever (optional):
    //
    while(1)

    {

       if(CAN_A_transmit_flag==1)
            {
            update_data_model();
            run_CAN_A();
            CAN_A_transmit_flag=0;
            }
       listen_CAN_B();

    }
}


__interrupt void cpu_timer0_isr(void)
{
    CpuTimer0.InterruptCount++;
   // ECanaRegs.CANTRR.bit.TRR25 = 1;
    time_ms=time_ms+10;//10ms timer interrupt
    if(time_ms==1000)
            {
            time_ms=0;//Reset every second
            time_s=time_s+1;
            if(time_s==3600)
                {time_s=0;}//Reset every hour
            CAN_A_transmit_flag=1;
            }
    TM3_Ctrl();
    // Acknowledge this interrupt to receive more interrupts from group 1
    //
    PieCtrlRegs.PIEACK.all = PIEACK_GROUP1;
}


